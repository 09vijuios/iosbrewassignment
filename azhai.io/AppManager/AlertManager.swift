

import UIKit

struct VAlert {
    let title: String!
    let message: String!
}

extension UIAlertController {
    func present() {
        var window = UIWindow()
//        if #available(iOS 13.0, *){
//            window = UIApplication.shared.windows.first!
//        }else{
            window = UIApplication.shared.keyWindow!
//        }
        //UIApplication.shared.keyWindow!
        if let modalVC = window.rootViewController?.presentedViewController {
            modalVC.present(self, animated: true, completion: nil)
        } else {
            window.rootViewController!.present(self, animated: true, completion: nil)
        }
    }
}

class AlertManager: NSObject, UIAlertViewDelegate {
    static let shared = AlertManager()
    private override init() {}
    private var isToastAdded = false
    var completionBlock: ((_ selectedIndex: Int) -> Void)? = nil
    
    func addToast(_ onView: UIView, message: String, duration: TimeInterval = 3.0) {
        guard !isToastAdded else {return}
        
        isToastAdded = !isToastAdded
        
        DispatchQueue.main.asyncAfter(deadline: .now()+duration) {
            self.isToastAdded = !self.isToastAdded
        }
    }
    
    func show(_ alert: VAlert
        , buttonsArray : [Any] = ["OK"]
        , completionBlock : ((_ : Int) -> ())? = nil) {

        self.completionBlock = completionBlock
        let alertView = UIAlertController(title: alert.title, message: alert.message, preferredStyle: .alert)
        for i in 0..<buttonsArray.count {
            let buttonTitle: String = buttonsArray[i] as! String
            let btnAction = UIAlertAction(title: buttonTitle, style: .default) { (alertAction) in
                if self.completionBlock != nil {
                    self.completionBlock!(i)
                }
            }
            alertView.view.tintColor = UIColor.init(red: 50/255, green: 71/255, blue: 230/255, alpha: 1)
            alertView.addAction(btnAction)
        }
        alertView.present()
        
    }
    
    func showWithoutLocalize(_ alert: VAlert
        , buttonsArray : [Any] = ["Ok"]
        , completionBlock : ((_ : Int) -> ())? = nil) {
        
        self.completionBlock = completionBlock
        
        let alertView = UIAlertController(title: alert.title, message: alert.message, preferredStyle: .alert)
        for i in 0..<buttonsArray.count {
            let buttonTitle: String = buttonsArray[i] as! String
            let btnAction = UIAlertAction(title: buttonTitle, style: .default) { (alertAction) in
                if self.completionBlock != nil {
                    self.completionBlock!(i)
                }
            }
            alertView.addAction(btnAction)
        }
        
        alertView.view.tintColor = UIColor.init(red: 50/255, green: 71/255, blue: 230/255, alpha: 1)
        
        alertView.present()
    }
    
    func showPopup(_ alert : VAlert
        , forTime time : Double
        , completionBlock : ((_ : Int) -> ())? = nil) {
    
        self.completionBlock = completionBlock
        
        let alertView = UIAlertController(title: alert.title, message: alert.message, preferredStyle: .alert)
        alertView.present()
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double((ino64_t)(time * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {() -> Void in
            alertView.dismiss(animated: true)
            if completionBlock != nil {
                completionBlock!(0)
            }
        })
        
    }
    
    func showActionSheet(_ sender: UIView, alert: VAlert, buttonsArray : [Any] = ["Ok"], completionBlock : ((_ : Int) -> ())? = nil) {
        
        self.completionBlock = completionBlock
        
        let alert = UIAlertController(title: alert.title, message: alert.message, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        for i in 0..<buttonsArray.count {
            let buttonTitle: String = buttonsArray[i] as! String
            let btnAction = UIAlertAction(title: buttonTitle, style: .default) { (alertAction) in
                if self.completionBlock != nil {
                    self.completionBlock!(i)
                }
            }
            alert.addAction(btnAction)
        }
        alert.view.tintColor = UIColor.init(red: 50/255, green: 71/255, blue: 230/255, alpha: 1)
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender
            alert.popoverPresentationController?.sourceRect = sender.bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        alert.present()
    }
    
    
}

class GNavigation: NSObject {
    static let shared = GNavigation()
    
   
    
    var MainStoryBoard = UIStoryboard(name: "Main", bundle: .main)
    var NavigationController: UINavigationController!
    
    func push(_ vc: UIViewController, isAnimated: Bool = true) {
        self.NavigationController.pushViewController(vc, animated: isAnimated)
    }
    
    func pop(_ isAnimated: Bool = true) {
        self.NavigationController.popViewController(animated: isAnimated)
    }
    
    func present(_ vc: UIViewController, isAnimated: Bool = false) {
        if #available(iOS 13.0, *) {
           UIApplication.shared.windows.first?.rootViewController?.navigationController?.present(vc, animated: true, completion: nil)
        } else {
            // Fallback on earlier versions
            AppDelegate.sharedDelegate.navigation?.present(vc, animated: true, completion: nil)
        }
        
    }
    
    func dismiss(_ isAnimated: Bool = false, completion: @escaping () -> Void = { }) {
        self.NavigationController.dismiss(animated: isAnimated, completion: {
            completion()
        })
    }
    
    
    func popToRoot(_ vcName: AnyClass, isAnimated: Bool = true) {
        let controllers = self.NavigationController.viewControllers
        for vc in controllers {
            if vc.isKind(of: vcName) {
                self.NavigationController.popToViewController(vc, animated: isAnimated)
            }
        }
    }
    
}

