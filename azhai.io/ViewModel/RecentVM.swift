//
//  RecentVM.swift
//  azhai.io
//
//  Created by Vijay Verma on 21/03/23.
//

import UIKit

class RecentVM: NSObject {
     
}

extension RecentVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isAll{
            return 10
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellRecentContactListTableViewCell", for: indexPath) as! CellRecentContactListTableViewCell
        if !self.isAll{
            cell.lblTitle.textColor = UIColor.init(red: 248/255, green: 75/255, blue: 75/255, alpha: 1)
            cell.imgCallstatus.image = UIImage.init(named: "call_ended")
            cell.lblAboutTitle.text = "Missed Call"
            if isLongPress{
                cell.btnInfo.isHidden = true
                cell.btnDelete.isHidden = false
            }else{
                cell.btnInfo.isHidden = false
                cell.btnDelete.isHidden = true
            }
        }else{
            cell.btnInfo.isHidden = false
            cell.btnDelete.isHidden = true
            if indexPath.row == 0{
                cell.lblTitle.textColor = UIColor.init(red: 248/255, green: 75/255, blue: 75/255, alpha: 1)
                cell.imgCallstatus.image = UIImage.init(named: "call_ended")
                cell.lblAboutTitle.text = "Missed Call"
            }else{
                cell.lblTitle.textColor = UIColor.black
                cell.imgCallstatus.image = UIImage.init(named: "call_incoming")
                cell.lblAboutTitle.text = "Incoming"
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            success(true)
        })
        deleteAction.image = UIImage.init(named: "delete")
        deleteAction.backgroundColor = .red
        
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.instantiateVC(with: "ContactDetailVC") as! ContactDetailVC
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func handleLongPress(sender: UILongPressGestureRecognizer) {
        if sender.state == .began {
            if !self.isAll{
                let touchPoint = sender.location(in: tblData)
                if let indexPath = tblData.indexPathForRow(at: touchPoint) {
                    debugPrint(indexPath)
                    self.isLongPress = true
                    self.tblData.reloadData()
                }
            }
        }
    }
    
    
}
