//
//  AccountVM.swift
//  azhai.io
//
//  Created by Vijay Verma on 21/03/23.
//

import UIKit

class AccountVM: NSObject {

}
extension AccountVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellAccount", for: indexPath) as! CellAccount
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        switch indexPath.row{
        case 0:
            cell.imgProfile.image = UIImage.init(named: "get_another_number")
            cell.lblTitle.text = "Get another Number"
            cell.lblAboutTitle.text = "Adding another number to your account"
        case 1:
            cell.imgProfile.image = UIImage.init(named: "usage_history")
            cell.lblTitle.text = "Usage History"
            cell.lblAboutTitle.text = "Show detailed usage history"
        case 2:
            cell.imgProfile.image = UIImage.init(named: "saved_cards")
            cell.lblTitle.text = "Saved Cards"
            cell.lblAboutTitle.text = "Saved credit / Debit cards for future payments"
        case 3:
            cell.imgProfile.image = UIImage.init(named: "settings")
            cell.lblTitle.text = "Settings"
            cell.lblAboutTitle.text = "Call forwarding, Call recording, Add desk phone"
        default:
            break
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 3{
            let vc = self.instantiateVC(with: "SeetingsVC") as! SeetingsVC
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 1{
            let vc = self.instantiateVC(with: "UsagesHistoryVC") as! UsagesHistoryVC
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
}
