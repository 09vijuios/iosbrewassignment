//
//  ChooseYouPlanVM.swift
//  azhai.io
//
//  Created by Vijay Verma on 24/03/23.
//

import UIKit

class ChooseYouPlanVM: NSObject {

    var selectedIndex = 0
    var arrOpenIndex = [Int]()
}

extension ChooseYouPlanVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellChoosePlan", for: indexPath) as! CellChoosePlan
        cell.backgroundColor = .clear
        cell.viewDetail.isHidden = true
        cell.lblDetailAboutTitle.text = "◉ Unlimited Calls\n\n◉ Business phone or toll-free numbers\n\n◉ Unlimited calls within the US/Canada\n\n◉ Unlimited business SMS\n\n◉ Voicemail-to-text\n\n◉ Team messaging\n\n◉ Document sharing"
        if viewModel.selectedIndex == indexPath.row{
            cell.lblTitle.textColor = .white
            cell.lblAboutTitle.textColor = .white
            cell.btnBenifit.setTitleColor(.white, for: .normal)
            cell.btnBenifit.setImage(UIImage.init(named: "dropdown_arrow_white"), for: .normal)
            cell.imgRadio.image = UIImage.init(named: "radio_active")
            cell.viewGredient.isHidden = false
        }else{
            cell.lblTitle.textColor = UIColor.init(red: 113/255, green: 113/255, blue: 113/255, alpha: 1)
            cell.lblAboutTitle.textColor = .black
            cell.btnBenifit.setTitleColor(UIColor.init(red: 113/255, green: 113/255, blue: 113/255, alpha: 1), for: .normal)
            cell.btnBenifit.setImage(UIImage.init(named: "dropdown_arrow_grey"), for: .normal)
            cell.imgRadio.image = UIImage.init(named: "radio_inactive")
            cell.viewGredient.isHidden = true
        }
        
        if self.viewModel.arrOpenIndex.contains(indexPath.row){
            cell.viewDetail.isHidden = false
        }else{
            cell.viewDetail.isHidden = true
        }
        cell.btnBenifit.tag = indexPath.row
        cell.btnBenifit.addTarget(self, action: #selector(btnShowDetailAction(btn: )), for: .touchUpInside)
        
        if indexPath.row == 1{
            cell.imgPopular.isHidden = false
        }else{
            cell.imgPopular.isHidden = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewModel.selectedIndex = indexPath.row
        self.tblData.reloadData()
    }
    
    @objc func btnShowDetailAction(btn: UIButton){
        if self.viewModel.arrOpenIndex.contains(btn.tag) {
            self.viewModel.arrOpenIndex = self.viewModel.arrOpenIndex.filter { $0 != btn.tag}
        }
        else {
            self.viewModel.arrOpenIndex.append(btn.tag)
        }
        self.tblData.reloadData()
    }
    
    
}
