//
//  SeetingsVM.swift
//  azhai.io
//
//  Created by Vijay Verma on 27/03/23.
//

import UIKit

class SeetingsVM: NSObject {

}

extension SeetingsVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellAccount", for: indexPath) as! CellAccount
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        switch indexPath.row{
        case 0:
            cell.imgProfile.image = UIImage.init(named: "call_forward")
            cell.lblTitle.text = "Call Forward"
            cell.lblAboutTitle.text = "Forward all your calls"
        case 1:
            cell.imgProfile.image = UIImage.init(named: "call_recording")
            cell.lblTitle.text = "Call Recording"
            cell.lblAboutTitle.text = "Show Call recordings"
        case 2:
            cell.imgProfile.image = UIImage.init(named: "add_desk_phone")
            cell.lblTitle.text = "Add a desk phone"
            cell.lblAboutTitle.text = "New desk phone setup"
        default:
            break
        }
        return cell
    }
    
    
}
