//
//  SignVM.swift
//  azhai.io
//
//  Created by Vijay Verma on 27/03/23.
//

import UIKit

class SignInVM: NSObject {

}

extension SignInVC{
    
    
    func presentCountryPickerScene() {
        // Present country picker with `Section Control` enabled
        CountryPickerWithSectionViewController.presentController(on: self, configuration: { countryController in
            countryController.configuration.flagStyle = .normal
            countryController.configuration.isCountryFlagHidden = false
            countryController.configuration.isCountryDialHidden = false
//            countryController.favoriteCountriesLocaleIdentifiers = ["IN", "US"]
            
        }) { [weak self] country in
            guard let self = self else { return }
            self.lblCountry.text = country.dialingCode

        }
        
    }
}
