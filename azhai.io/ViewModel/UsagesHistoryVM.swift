//
//  UsagesHistoryVM.swift
//  azhai.io
//
//  Created by Vijay Verma on 29/03/23.
//

import UIKit

class UsagesHistoryVM: NSObject {

    var arrOpenIndex = [Int]()
}


extension UsagesHistoryVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellUsagesHistory", for: indexPath) as! CellUsagesHistory
        cell.backgroundColor = .clear
        cell.viewDetail.isHidden = true
//        cell.lblDetailAboutTitle.text = "◉ Unlimited Calls\n\n◉ Business phone or toll-free numbers\n\n◉ Unlimited calls within the US/Canada\n\n◉ Unlimited business SMS\n\n◉ Voicemail-to-text\n\n◉ Team messaging\n\n◉ Document sharing"
        
        if self.viewModel.arrOpenIndex.contains(indexPath.row){
            cell.viewDetail.isHidden = false
        }else{
            cell.viewDetail.isHidden = true
        }
        cell.heightViewDetail.constant = cell.tblUserHistory.contentSize.height + 15
        cell.layoutIfNeeded()
        cell.btnView.tag = indexPath.row
        cell.btnView.addTarget(self, action: #selector(btnShowDetailAction(btn: )), for: .touchUpInside)
        return cell
    }
    
    @objc func btnShowDetailAction(btn: UIButton){
        if self.viewModel.arrOpenIndex.contains(btn.tag) {
            self.viewModel.arrOpenIndex = self.viewModel.arrOpenIndex.filter { $0 != btn.tag}
        }
        else {
            self.viewModel.arrOpenIndex.append(btn.tag)
        }
        self.tblData.reloadData()
    }
    
    
}
