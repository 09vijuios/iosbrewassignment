//
//  VoiceCallVM.swift
//  azhai.io
//
//  Created by Vijay Verma on 21/03/23.
//

import UIKit

class VoiceCallVM: NSObject {

}

extension VoiceCallVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellVoiceCall", for: indexPath) as! CellVoiceCall
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            success(true)
            
            let vc = self.instantiateVC(with: "SignUpPopUpVC") as! SignUpPopUpVC
            vc.modalPresentationStyle = .popover
            vc.completion = {
                let vcS = self.instantiateVC(with: "SignUpVC") as! SignUpVC
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vcS, animated: true)
            }
            self.present(vc, animated: true)
        })
        deleteAction.image = UIImage.init(named: "delete")
        deleteAction.backgroundColor = .red
        
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.instantiateVC(with: "SignUpPopUpVC") as! SignUpPopUpVC
        vc.modalPresentationStyle = .popover
        vc.completion = {
            let vcS = self.instantiateVC(with: "SignUpVC") as! SignUpVC
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vcS, animated: true)
        }
        
        vc.completionSignIN = {
            let vcS = self.instantiateVC(with: "SignInVC") as! SignInVC
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vcS, animated: true)
        }
        self.present(vc, animated: true)
    }
    
}
