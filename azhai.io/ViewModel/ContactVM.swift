//
//  ContactVM.swift
//  azhai.io
//
//  Created by Vijay Verma on 21/03/23.
//

import UIKit

class ContactVM: NSObject {

    var arrName = ["Vijay", "Ajay", "Jay"]
}

extension ContactVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.arrName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellContact", for: indexPath) as! CellContact
        cell.lblName.text = viewModel.arrName[indexPath.row]
        return cell
    }
    
//    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
//        debugPrint(viewModel.arrName.map {String($0)})
//        return viewModel.arrName.map {String($0)}
//    }
//
//    func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
//        return viewModel.arrName.firstIndex(of: String(title))!
//    }
    
}
