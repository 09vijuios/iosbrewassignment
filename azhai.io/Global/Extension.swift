//
//  Extension.swift
//  azhai.io
//
//  Created by Vijay Verma on 21/03/23.
//

import UIKit

extension UIView{
    func setGradientBackground() {
        let colorTop =  UIColor(red: 133.0/255.0, green: 175.0/255.0, blue: 253.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 80.0/255.0, green: 86.0/255.0, blue: 186.0/255.0, alpha: 1.0).cgColor
                    
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.startPoint = CGPoint(x: 1.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 1.0)
        gradientLayer.frame = self.bounds
                
        self.layer.insertSublayer(gradientLayer, at:0)
    }
}
extension UINavigationController {
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

extension NSObject{
    func instantiateVC(with identifier: String) -> UIViewController {
        let MainStoryBoard = UIStoryboard(name: "Main", bundle: .main)
        return MainStoryBoard.instantiateViewController(withIdentifier: identifier)
    }
}

extension UIFont{
//    class func appBoldFontWith( size: CGFloat)-> UIFont{
//        return UIFont(name: "SF-Pro-Display-Bold", size: size) ?? UIFont()
//    }
//
//    class func appRegularFontWith( size: CGFloat)-> UIFont{
//        return UIFont(name: "SF-Pro-Display-Regular", size: size) ?? UIFont()
//    }
//
//    class func appMediumFontWith( size: CGFloat)-> UIFont{
//        return UIFont(name: "SF-Pro-Display-Medium", size: size) ?? UIFont()
//    }
//
//    class func appLightFontWith( size: CGFloat)-> UIFont{
//        return UIFont(name: "SF-Pro-Display-Light", size: size) ?? UIFont()
//    }
}
