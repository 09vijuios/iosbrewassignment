//
//  ContactDetailVC.swift
//  azhai.io
//
//  Created by Vijay Verma on 21/03/23.
//

import UIKit

class ContactDetailVC: UIViewController {

    @IBOutlet weak var topView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.UIConfigure()
        // Do any additional setup after loading the view.
    }
    
    func UIConfigure(){
        self.topView.setGradientBackground()
    }
    
    @IBAction func btnEditAction(_ sender: Any) {
        
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnBlockAction(_ sender: Any) {
        
    }

}
