//
//  SignUpPopUpVC.swift
//  azhai.io
//
//  Created by Vijay Verma on 22/03/23.
//

import UIKit

class SignUpPopUpVC: UIViewController {

    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var btnSignUp: UIButton!
    
    typealias COMPLETION = ()-> Void
    var completion: COMPLETION?
    var completionSignIN: COMPLETION?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.btnSignUp.setGradientBackground()
        self.bottomView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        // Do any additional setup after loading the view.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        var touch: UITouch? = touches.first
        if touch?.view == self.view {
            dismiss(animated: true, completion: nil)
        }
    }
    @IBAction func btnCloseAction(_ sender: Any) {
        self.dismiss(animated: true)
    }
    @IBAction func btnSingUpAction(_ sender: Any) {
        self.dismiss(animated: true) {
            if self.completion != nil{
                self.completion!()
            }
        }
    }
    
    @IBAction func btnSingInAction(_ sender: Any) {
        self.dismiss(animated: true) {
            if self.completionSignIN != nil{
                self.completionSignIN!()
            }
        }
    }

}
