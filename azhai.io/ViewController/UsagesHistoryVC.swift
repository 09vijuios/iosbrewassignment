//
//  UsagesHistoryVC.swift
//  azhai.io
//
//  Created by Vijay Verma on 29/03/23.
//

import UIKit

class UsagesHistoryVC: UIViewController {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var tblData: UITableView!
    
    var viewModel = UsagesHistoryVM()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.topView.setGradientBackground()
        
        //Register table cell
        self.tblData.register(UINib(nibName: "CellUsagesHistory", bundle: nil), forCellReuseIdentifier: "CellUsagesHistory")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
