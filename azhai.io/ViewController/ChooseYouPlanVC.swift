//
//  ChooseYouPlanVC.swift
//  azhai.io
//
//  Created by Vijay Verma on 24/03/23.
//

import UIKit

class ChooseYouPlanVC: UIViewController {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var btnMake: UIButton!
    @IBOutlet weak var tblData: UITableView!
    
    var viewModel = ChooseYouPlanVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.topView.setGradientBackground()
        self.btnMake.setGradientBackground()
        
        //Register table cell
        self.tblData.register(UINib(nibName: "CellChoosePlan", bundle: nil), forCellReuseIdentifier: "CellChoosePlan")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func btnMakeAction(_ sender: Any) {
        let vc = self.instantiateVC(with: "AddCardVC") as! AddCardVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

}
