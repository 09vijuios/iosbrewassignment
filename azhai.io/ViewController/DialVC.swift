//
//  DialVC.swift
//  azhai.io
//
//  Created by Vijay Verma on 21/03/23.
//

import UIKit

class DialVC: UIViewController{

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var viewDial: UIView!
    @IBOutlet var viewNumber: [UIView]!
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        self.textField.inputView = viewDial
//        self.textField.isUserInteractionEnabled = true
        self.topView.setGradientBackground()
        self.setUI()
        // Do any additional setup after loading the view.
    }
    
    func setUI(){
        for btn in viewNumber{
            btn.layer.cornerRadius = btn.frame.height / 2
            btn.clipsToBounds = true
        }
    }
    
    @IBAction func btnNumberAction(_ sender: UIButton) {
        //Tag 0 to 9 and for * = 10, # = 11
        switch sender.tag{
        case 0:
            textField.text = "\(textField.text ?? "")0"
        case 1:
            textField.text = "\(textField.text ?? "")1"
        case 2:
            textField.text = "\(textField.text ?? "")2"
        case 3:
            textField.text = "\(textField.text ?? "")3"
        case 4:
            textField.text = "\(textField.text ?? "")4"
        case 5:
            textField.text = "\(textField.text ?? "")5"
        case 6:
            textField.text = "\(textField.text ?? "")6"
        case 7:
            textField.text = "\(textField.text ?? "")7"
        case 8:
            textField.text = "\(textField.text ?? "")8"
        case 9:
            textField.text = "\(textField.text ?? "")9"
        case 10:
            textField.text = "\(textField.text ?? "")*"
        case 11:
            textField.text = "\(textField.text ?? "")#"
        default:
            break
        }
    }
    
    @IBAction func btnCallAction(_ sender: UIButton) {
    }
    
    @IBAction func btnCancelAction(_ sender: UIButton) {
        if (textField.text?.count ?? 0) > 0{
            let str = textField.text ?? ""
            textField.text = String(str.dropLast())
        }
    }
    
    

}
