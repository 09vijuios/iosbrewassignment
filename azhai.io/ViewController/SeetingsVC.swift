//
//  SeetingsVC.swift
//  azhai.io
//
//  Created by Vijay Verma on 27/03/23.
//

import UIKit

class SeetingsVC: UIViewController {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var tblData: UITableView!
    
    
    var viewModel = AccountVM()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.UIConfigure()
        // Do any additional setup after loading the view.
    }
    
    func UIConfigure(){
        self.topView.setGradientBackground()
        
        //Register table cell
        self.tblData.register(UINib(nibName: "CellAccount", bundle: nil), forCellReuseIdentifier: "CellAccount")
    }
    
    @IBAction func btnEditAction(_ sender: Any) {
        
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
