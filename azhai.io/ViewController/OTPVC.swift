//
//  OTPVC.swift
//  azhai.io
//
//  Created by Vijay Verma on 23/03/23.
//

import UIKit

class OTPVC: UIViewController {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var btnSign: UIButton!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var otpView: OTPFieldView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.topView.setGradientBackground()
        self.btnSign.setGradientBackground()
        self.setOtpView()
        // Do any additional setup after loading the view.
    }
    
    func setOtpView(){
        self.otpView.delegate = self
        self.otpView.cursorColor = .blue
        self.otpView.displayType = .square
        self.otpView.fieldsCount = 5
        self.otpView.fieldSize = 50
        self.otpView.initializeUI()
        self.otpView.becomeFirstResponder()
//        self.otpView.backgroundColor = .lightGray
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnResentAction(_ sender: Any) {
        
    }
    
    @IBAction func btnSignUpAction(_ sender: Any) {
        let vc = self.instantiateVC(with: "ChooseYouPlanVC") as! ChooseYouPlanVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

}

extension OTPVC: OTPFieldViewDelegate{
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otp: String) {
        debugPrint(otp)
    }
    
    func hasEnteredAllOTP(hasEnteredAll: Bool) -> Bool {
        return true
    }
    
    
}
