//
//  AddCardVC.swift
//  azhai.io
//
//  Created by Vijay Verma on 24/03/23.
//

import UIKit

class AddCardVC: UIViewController {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var btnSign: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.topView.setGradientBackground()
        self.btnSign.setGradientBackground()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSignUpAction(_ sender: Any) {
        AlertManager.shared.show(VAlert(title: "Save Card Data", message: "You can save your card information for future payments. Instantly make payments just by selecting your saved cards details")) { i in
            //        let vc = self.instantiateVC(with: "OTPVC") as! OTPVC
            //        self.navigationController?.pushViewController(vc, animated: true)
        }

    }
    
    @IBAction func btnTickAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func btnInfoAction(_ sender: Any) {
        
    }
    

}
