//
//  ContactVC.swift
//  azhai.io
//
//  Created by Vijay Verma on 21/03/23.
//

import UIKit

class ContactVC: UIViewController {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var btnAll: UIButton!
    @IBOutlet weak var btnM: UIButton!
    @IBOutlet weak var lblRecent: UILabel!
    @IBOutlet weak var btnSync: UIButton!
    @IBOutlet weak var dataView: UIView!
    @IBOutlet weak var tblData: UITableView!
    @IBOutlet weak var viewSearch: UIView!
    
    var isAll = true
    var viewModel = ContactVM()
    var isLongPress = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.UIConfigure()
    }
    
    func UIConfigure(){
        self.setSegmentUI(ststus: true)
        self.topView.setGradientBackground()
        self.btnSync.setGradientBackground()
        self.lblRecent.textColor = .white
        self.dataView.isHidden = true
        //Register table cell
        self.tblData.register(UINib(nibName: "CellContact", bundle: nil), forCellReuseIdentifier: "CellContact")
        viewModel.arrName = viewModel.arrName.sorted()
        self.viewSearch.isHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }

    @IBAction func btnEditAction(_ sender: Any) {
        
    }
    
    
    @IBAction func btnSyncAction(_ sender: Any) {
        self.dataView.isHidden = false
        self.viewSearch.isHidden = false
    }
    
    @IBAction func btnAllAction(_ sender: Any) {
        self.setSegmentUI(ststus: true)
    }
    @IBAction func btnMishedAction(_ sender: Any) {
        self.setSegmentUI(ststus: false)
    }
    
    func setSegmentUI(ststus: Bool){
        if ststus{
            self.isAll = true
            self.btnAll.tintColor = UIColor.init(red: 81/255, green: 100/255, blue: 167/255, alpha: 1)
            self.btnM.tintColor = UIColor.white
            self.btnAll.setTitleColor(UIColor.init(red: 81/255, green: 100/255, blue: 167/255, alpha: 1), for: .normal)
            self.btnM.setTitleColor(UIColor.white, for: .normal)
            self.btnM.backgroundColor = .clear
            self.btnAll.backgroundColor = .white
        }else{
            self.isAll = false
            self.btnAll.tintColor = .white
            self.btnM.tintColor = UIColor.init(red: 81/255, green: 100/255, blue: 167/255, alpha: 1)
            self.btnM.setTitleColor(UIColor.init(red: 81/255, green: 100/255, blue: 167/255, alpha: 1), for: .normal)
            self.btnAll.setTitleColor(.white, for: .normal)
            self.btnAll.backgroundColor = .clear
            self.btnM.backgroundColor = .white
        }
        self.isLongPress = false
//        self.tblData.reloadData()
    }
}
