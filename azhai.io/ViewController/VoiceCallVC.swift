//
//  VoiceCallVC.swift
//  azhai.io
//
//  Created by Vijay Verma on 21/03/23.
//

import UIKit

class VoiceCallVC: UIViewController {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var lblRecent: UILabel!
    @IBOutlet weak var tblListVoice: UITableView!
    @IBOutlet weak var viewData: UIView!
    
    
    var viewModel = VoiceCallVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.UIConfigure()
        self.viewData.isHidden = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.viewData.isHidden = false
        }
    }
    
    func UIConfigure(){
        self.topView.setGradientBackground()
        self.lblRecent.textColor = .white
        
        //Register table cell
        self.tblListVoice.register(UINib(nibName: "CellVoiceCall", bundle: nil), forCellReuseIdentifier: "CellVoiceCall")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }

    @IBAction func btnEditAction(_ sender: Any) {
        
    }
    
}
