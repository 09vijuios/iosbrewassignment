//
//  SignUpVC.swift
//  azhai.io
//
//  Created by Vijay Verma on 22/03/23.
//

import UIKit

class SignUpVC: UIViewController {

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var btnSign: UIButton!
    @IBOutlet weak var lblCountry: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.topView.setGradientBackground()
        self.btnSign.setGradientBackground()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDropAction(_ sender: Any) {
        CountryManager.shared.addFilter(.countryName)
        presentCountryPickerScene()
    }
    
    @IBAction func btnSignUpAction(_ sender: Any) {
        let vc = self.instantiateVC(with: "OTPVC") as! OTPVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnSingInAction(_ sender: Any) {
        let vc = self.instantiateVC(with: "SignInVC") as! SignInVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

}


private extension SignUpVC {
    
    
    func presentCountryPickerScene() {
        // Present country picker with `Section Control` enabled
        CountryPickerWithSectionViewController.presentController(on: self, configuration: { countryController in
            countryController.configuration.flagStyle = .normal
            countryController.configuration.isCountryFlagHidden = false
            countryController.configuration.isCountryDialHidden = false
//            countryController.favoriteCountriesLocaleIdentifiers = ["IN", "US"]
            
        }) { [weak self] country in
            guard let self = self else { return }
            self.lblCountry.text = country.dialingCode

        }
        
    }
}
