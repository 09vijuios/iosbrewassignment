//
//  AppDelegate.swift
//  azhai.io
//
//  Created by Vijay Verma on 21/03/23.
//

import UIKit


@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var navigation: UINavigationController?
    
    var tabBarController : UITabBarController? = UITabBarController()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        
        
        if #available(iOS 13.0, *) {
            UIApplication.shared.windows.first?.overrideUserInterfaceStyle = .light
        }else{
            //Fallback on earlier versions
            
        }
        
        DispatchQueue.main.async {
            self.setupCustomTabbar()
        }
        
        // Override point for customization after application launch.
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    //MARK: shared Instance
    class var sharedDelegate:AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }


}



extension AppDelegate: UITabBarControllerDelegate, UITabBarDelegate{
    //MARK: Set up TabBar

    func setupCustomTabbar() {
        
        guard let recentvc = self.instantiateVC(with: "RecentVC") as? RecentVC else {return}
        recentvc.tabBarItem.image = UIImage.init(named: "navicon_recents_inactive")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        recentvc.tabBarItem.selectedImage = UIImage.init(named: "navicon_recents_active")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        recentvc.tabBarItem.imageInsets =  UIEdgeInsets.init(top: 0, left: 0, bottom: -10, right: 0)

        guard let contactvc = self.instantiateVC(with: "ContactVC") as? ContactVC else {return}
        contactvc.tabBarItem.image = UIImage.init(named: "navicon_contacts_inactive")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        contactvc.tabBarItem.selectedImage = UIImage.init(named: "navicon_contacts_active")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        contactvc.tabBarItem.imageInsets =  UIEdgeInsets.init(top: 0, left: 0, bottom: -10, right: 0)

        guard let dialvc = self.instantiateVC(with: "DialVC") as? DialVC else {return}
        dialvc.tabBarItem.image = UIImage.init(named: "navicon_dialpad_inactive")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        dialvc.tabBarItem.selectedImage = UIImage.init(named: "navicon_dialpad_active")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        dialvc.tabBarItem.imageInsets =  UIEdgeInsets.init(top: 0, left: 0, bottom: -10, right: 0)

        guard let voicevc = self.instantiateVC(with: "VoiceCallVC") as? VoiceCallVC else {return}
        voicevc.tabBarItem.image = UIImage.init(named: "navicon_voicemail_inactive")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        voicevc.tabBarItem.selectedImage = UIImage.init(named: "navicon_voicemail_active")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        voicevc.tabBarItem.imageInsets =  UIEdgeInsets.init(top: 0, left: 0, bottom: -10, right: 0)
        
        guard let accountVC = self.instantiateVC(with: "AccountVC") as? AccountVC else {return}
        accountVC.tabBarItem.image = UIImage.init(named: "navicon_profile_inactive")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        accountVC.tabBarItem.selectedImage = UIImage.init(named: "navicon_profile_active")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        accountVC.tabBarItem.imageInsets =  UIEdgeInsets.init(top: 0, left: 0, bottom: -10, right: 0)
        

        //Set Navigation
        let taskNavigation : UINavigationController = UINavigationController(rootViewController: recentvc)
        let BraweNavigation : UINavigationController = UINavigationController(rootViewController: contactvc)
        let myTaskNavigation : UINavigationController = UINavigationController(rootViewController: dialvc)
        let messageNavigation : UINavigationController = UINavigationController(rootViewController: voicevc)
        let accountNavigation : UINavigationController = UINavigationController(rootViewController: accountVC)

        //Hidden Navigation Bar
        taskNavigation.navigationBar.isHidden   = true
        BraweNavigation.navigationBar.isHidden = true
        myTaskNavigation.navigationBar.isHidden   = true
        messageNavigation.navigationBar.isHidden   = true
        accountNavigation.navigationBar.isHidden   = true

        //Set TabBarViewController
        self.tabBarController?.viewControllers = [taskNavigation , BraweNavigation, myTaskNavigation, messageNavigation, accountNavigation]

        //Set Tabbar Layout
        self.tabBarController?.tabBar.backgroundColor = .white
        self.tabBarController?.tabBar.barTintColor = .white
        
        //Set Corner Radius of tabbar
        if #available(iOS 11.0, *) {
            self.tabBarController?.tabBar.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        } else {
            // Fallback on earlier versions
        }
        
//        self.tabBarController?.tabBar.cornerRadius = 20
        self.tabBarController?.delegate = self
        
        
        //Add shadow
        
        self.tabBarController?.tabBar.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.tabBarController?.tabBar.layer.shadowRadius = 3
        self.tabBarController?.tabBar.layer.shadowColor = UIColor.black.cgColor
        self.tabBarController?.tabBar.layer.shadowOpacity = 0.3
        self.tabBarController?.delegate = self
        self.tabBarController?.tabBar.isTranslucent = true
        

        if #available(iOS 13.0, *) {
            UIApplication.shared.windows.first?.rootViewController = self.tabBarController!
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        } else {
            UIApplication.shared.keyWindow?.rootViewController = self.tabBarController!
            UIApplication.shared.keyWindow?.makeKeyAndVisible()
        }
    }
}

