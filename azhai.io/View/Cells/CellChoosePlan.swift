//
//  CellChoosePlan.swift
//  azhai.io
//
//  Created by Vijay Verma on 24/03/23.
//

import UIKit

class CellChoosePlan: UITableViewCell {

    @IBOutlet weak var imgPopular: UIImageView!
    @IBOutlet weak var btnBenifit: UIButton!
    @IBOutlet weak var lblAboutTitle: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgRadio: UIImageView!
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var viewGredient: UIView!
    @IBOutlet weak var lblDetailTitle: UILabel!
    @IBOutlet weak var lblDetailAboutTitle: UILabel!
    @IBOutlet weak var viewDetail: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewGredient.setGradientBackground()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
