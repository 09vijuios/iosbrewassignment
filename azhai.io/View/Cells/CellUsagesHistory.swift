//
//  CellUsagesHistory.swift
//  azhai.io
//
//  Created by Vijay Verma on 29/03/23.
//

import UIKit

class CellUsagesHistory: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAboutTitle: UILabel!
    @IBOutlet weak var viewDetail: UIView!
    @IBOutlet weak var viewProgress: UIView!
    @IBOutlet weak var btnView: UIButton!
    @IBOutlet weak var heightViewDetail: NSLayoutConstraint!
    @IBOutlet weak var tblUserHistory: UITableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.registerCell()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func registerCell(){
        //Register table cell
        self.tblUserHistory.register(UINib(nibName: "CellUsagesInnerHeader", bundle: nil), forCellReuseIdentifier: "CellUsagesInnerHeader")
        self.tblUserHistory.register(UINib(nibName: "CellInnerUsagesHistory", bundle: nil), forCellReuseIdentifier: "CellInnerUsagesHistory")
    }
}

extension CellUsagesHistory: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellInnerUsagesHistory", for: indexPath) as! CellInnerUsagesHistory
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewHeader = tableView.dequeueReusableCell(withIdentifier: "CellUsagesInnerHeader") as! CellUsagesInnerHeader
        return viewHeader
    }
    
    
}
