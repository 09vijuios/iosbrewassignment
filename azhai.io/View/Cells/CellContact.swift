//
//  CellContact.swift
//  azhai.io
//
//  Created by Vijay Verma on 23/03/23.
//

import UIKit

class CellContact: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
