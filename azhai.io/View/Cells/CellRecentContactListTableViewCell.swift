//
//  CellRecentContactListTableViewCell.swift
//  azhai.io
//
//  Created by Vijay Verma on 21/03/23.
//

import UIKit

class CellRecentContactListTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAboutTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var imgCallstatus: UIImageView!
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var btnInfo: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
